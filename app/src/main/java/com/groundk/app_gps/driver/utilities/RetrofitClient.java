package com.groundk.app_gps.driver.utilities;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    //TODO: Provide instance of Retrofit

    public static final String BASE_URL = "http://210.115.182.242:8080/groundk/";
    public static final String PRO_BASE_URL = "http://www.groundk-driver/groundk/";

    private static Retrofit retrofit = null;



    public static Retrofit getClient(){
        if(retrofit==null){

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }

        return retrofit;
    }



}
