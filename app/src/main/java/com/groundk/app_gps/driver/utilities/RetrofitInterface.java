package com.groundk.app_gps.driver.utilities;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitInterface {


    //TODO: Define HTTP operations here
    @POST("{fullUrl}/driver/gps/{company_code}/{driver_id}")
    Call<ResponseBody> sendGPS(@Path(value = "fullUrl", encoded = true) String fullUrl,
                               @Path("company_code") String companyCode,
                               @Path("driver_id") String driverId,
                               @Query("v_idx") int vIdx,
                               @Query("lat") String lat,
                               @Query("lng") String lng);




}
