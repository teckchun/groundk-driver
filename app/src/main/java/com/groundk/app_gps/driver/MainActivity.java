package com.groundk.app_gps.driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.groundk.app_gps.BuildConfig;
import com.groundk.app_gps.R;
import com.groundk.app_gps.driver.utilities.RetrofitClient;
import com.groundk.app_gps.driver.utilities.RetrofitInterface;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private Context context;
    private WebView webView;

    private BroadcastReceiver broadcastReceiver;
    private NotificationManager mNM;
    private int NOTIFICATION = 1;


    // google
    FusedLocationProviderClient client;


    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private boolean mAlreadyStartedService = false;
    private static String webUrl = "";
    // demo

//    private static final String webUrl = "https://appkit.me/android.html";

    //webView Permission
    private String mGeoLocationRequestOrigin;
    private GeolocationPermissions.Callback mGeoLocationCallBack;
    private static final int MY_PERMISSION_REQUEST_LOCATION = 100;

    // double click back
    boolean doubleBackToExitPressedOnce = false;

    // swip view
    private SwipeRefreshLayout swipe;


    // Start lifecycle methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //TODO: set status bar background color
        Window window = getWindow();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.setStatusBarColor(getColor(R.color.colorDark));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.colorDark));
            }
        }


        //TODO: Initialized WebView and Settings
        // action type 1 for load url from notification
        Intent intent = getIntent();

        if (intent.hasExtra("url")) {

            Log.e(TAG, intent.getExtras().getString("url"));
            String url = intent.getExtras().getString("url");
            WebAction(url);

        } else {
            // Do something else
            Log.e(TAG, "no intent");
            WebAction("");
        }


        webView = (WebView) findViewById(R.id.webview);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });



    }


    //TODO: Webview actions
    public void WebAction(String url) {
        Log.e(TAG, "WebAction");

        webView = (WebView) findViewById(R.id.webview);
        // popup lib

        //TODO: Set up web view
//        webViewOverlay = new WebViewOverlay(this);
        // set enable JS
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        String fcmKey = SharePrefManager.getInstance(MainActivity.this).getToken();

        //TODO: Check if FCM key exist
        /*webUrl = FirebaseInstanceId.getInstance().getToken()
                ==null?"https://hvly.kr/user":"https://hvly.kr/user?fcmkey="+FirebaseInstanceId.getInstance().getToken();
*/

        //webUrl = "https://hvly.kr/user/index.html?id=seven_call&device=android&devcie_version=" + webSettings.getUserAgentString();
        webUrl = "http://groundk.com/driver_app/";

        Log.e(TAG, "webUrl => " + webUrl);

        String localHTML = "file:///android_asset/getLocation.html";

        if (url != "") {
            webView.loadUrl(url);
        } else {
            webView.loadUrl(webUrl);
        }

        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        webView.addJavascriptInterface(this, "Android");

        webView.getSettings().setSupportMultipleWindows(true);
        webView.setWebViewClient(new WebViewClient() {
            public static final String INTENT_PROTOCOL_START = "intent:";
            public static final String INTENT_PROTOCOL_INTENT = "#Intent;";
            public static final String INTENT_PROTOCOL_END = ";end;";
            public static final String GOOGLE_PLAY_STORE_PREFIX = "market://details?id=com.kakao.talk";


            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {


                Log.e(TAG, "web url=> " + url);

                //TODO: Check phone call URL
                if (url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                    view.reload();
                    return true;
                }

                // TODO: Kakao share
                if (url.startsWith(INTENT_PROTOCOL_START)) {
                    final int customUrlStartIndex = INTENT_PROTOCOL_START.length();
                    final int customUrlEndIndex = url.indexOf(INTENT_PROTOCOL_INTENT);
                    if (customUrlEndIndex < 0) {
                        return false;
                    } else {
                        final String customUrl = url.substring(customUrlStartIndex, customUrlEndIndex);
                        try {
                            Log.e(TAG, "CUSTOM URL" + customUrl);
                            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(customUrl));
                            //getBaseContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(customUrl)));
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getBaseContext().startActivity(i);
                        } catch (Exception e) {
                            final int packageStartIndex = customUrlEndIndex + INTENT_PROTOCOL_INTENT.length();
                            final int packageEndIndex = url.indexOf(INTENT_PROTOCOL_END);

                            final String packageName = url.substring(packageStartIndex, packageEndIndex < 0 ? url.length() : packageEndIndex);
                            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_PREFIX + packageName));
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getBaseContext().startActivity(i);

                        }
                        return true;
                    }
                }


                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

            }

            @Override
            public void onPageFinished(WebView view, String url) {

                // stop swipe from loading
                swipe.setRefreshing(false);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                // TODO: when page Receive any error
                if (!_amIConnected()) {
                    // TODO: check if the internet connected
                    view.loadUrl("file:///android_asset/error.html");
                }
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, true);//注意个函数，// 第二个参数就是是否同意定位权限，// 第三个是是否希望内核记住
                // requestPermissions();
                requestPermissionWebView(origin, callback);
                super.onGeolocationPermissionsShowPrompt(origin, callback);

            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                // TODO: Custom JS Alert from webpage
                AlertDialog dialog = new AlertDialog.Builder(view.getContext()).
                        setTitle("Taxi App").
                        setMessage(message).
                        setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        }).create();
                dialog.show();
                result.confirm();
                return true;
            }
        });


    }


    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onResume() {

        super.onResume();
        webView.reload();


    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Step 1: Check Google Play services
     */
    private void _startStep1() {
        Log.d(TAG, "startStep1");
        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {
            //Passing null to indicate that it is executing for the first time.
            _startStep2(null);

        } else {
            Toast.makeText(getApplicationContext(), R.string.no_google_playservice_available, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDestroy() {
        //Stop location sharing service to app server.........
        Intent intent = new Intent(this,LocationMonitoringService.class);

        if(isMyServiceRunning(LocationMonitoringService.class)){
            intent.putExtra("status","off");

            stopService(intent);
            LocationMonitoringService.shouldContinue = true;
            mAlreadyStartedService = false;
            //Ends................................................
            // unregisterReceiver(broadcastReceiver);

        }

        super.onDestroy();

    }

    //TODO: handle double back exit app


    @Override
    public void onBackPressed() {


        if (doubleBackToExitPressedOnce) {
            Log.d(TAG, "Click once");
            webView.goBack();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        webView.goBack();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);


    }

    // End lifecycle methods

    /**
     * Step 2: Check & Prompt Internet connection
     */
    private Boolean _startStep2(DialogInterface dialog) {
        Log.d(TAG, "startStep2");
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            promptInternetConnect();
            return false;
        }
        if (dialog != null) {
            dialog.dismiss();
        }
        //Yes there is active internet connection. Next check Location is granted by user or not.
        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.

            _startStep3();
        } else {  //No user has not granted the permissions yet. Request now.
            requestPermissions();
        }
        return true;
    }

    /**
     * Step 3: Start the Location Monitor Service
     */
    private void _startStep3() {

        Log.d(TAG, "startStep3");
        //And it will be keep running until you close the entire application from task manager.
        //This method will executed only once.

        if (!mAlreadyStartedService) {
            Log.d(TAG, "!mAlreadyStartedService");
            // textView.setText(R.string.msg_location_service_started);

            //Start location sharing service to app server.........
            //startService(intent);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getApplication().startForegroundService(intent);

            } else {

                getApplication().startService(intent);

            }*/
            Intent intent = new Intent(this, LocationMonitoringService.class);


            if(!isMyServiceRunning(LocationMonitoringService.class)){
                intent.putExtra("status","on");
                startService(intent);
            }



            mAlreadyStartedService = true;

            //Ends................................................
        } else {
            Log.d(TAG, "mAlreadyStartedService---");

        }
    }


    //TODO: Check Connectivity
    private boolean _amIConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /**
     * Return the availability of GooglePlayServices
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    /**
     * Show A Dialog with button to refresh the internet state.
     */
    private void promptInternetConnect() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.title_alert_no_intenet);
        builder.setMessage(R.string.msg_alert_no_internet);

        String positiveText = getString(R.string.btn_label_refresh);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Block the Application Execution until user grants the permissions
                        if (_startStep2(dialog)) {
                            //Now make sure about location permission.
                            if (checkPermissions()) {
                                //Step 2: Start the Location Monitor Service
                                //Everything is there to start the service.
                                showSnackbar(R.string.msg_location_service_started,
                                        android.R.string.ok, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {


                                            }
                                        });
                                _startStep3();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }

                        }
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        String locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        //TODO: CHECK GPS
        if (locationProviders == null || locationProviders.equals("")) {

            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            return false;
        }

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }

    /**
     * Start permissions requests.
     */
    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }


    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    private void showSnackBarWithString(final String Message, final int actionStringId,
                                        View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                Message,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }


    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {


        Log.i(TAG, "onRequestPermissionResult");

        switch (requestCode) {
            case REQUEST_PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length <= 0) {
                    // If img_user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i(TAG, "User interaction was cancelled.");
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Permission granted, updates requested, starting location updates");
                    _startStep3();

                } else {
                    // Permission denied.

                    // Notify the img_user via a SnackBar that they have rejected a core permission for the
                    // app, which makes the Activity useless. In a real app, core permissions would
                    // typically be best requested during a welcome-screen flow.

                    // Additionally, it is important to remember that a permission might have been
                    // rejected without asking the img_user for permission (device policy or "Never ask
                    // again" prompts). Therefore, a img_user interface affordance is typically implemented
                    // when permissions are denied. Otherwise, your app could appear unresponsive to
                    // touches or interactions which have required permissions.
                    showSnackbar(R.string.permission_denied_explanation,
                            R.string.settings, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // Build intent that displays the App settings screen.
                                    Intent intent = new Intent();
                                    intent.setAction(
                                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null);
                                    intent.setData(uri);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            });
                }
                return;
            }


            case MY_PERMISSION_REQUEST_LOCATION: {
                // If request is cancelled , the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    if (mGeoLocationCallBack != null) {
                        mGeoLocationCallBack.invoke(mGeoLocationRequestOrigin, true, true);
                    }
                } else {
                    // permission denied
                    if (mGeoLocationCallBack != null) {
                        mGeoLocationCallBack.invoke(mGeoLocationRequestOrigin, false, false);
                    }
                }
                return;
            }


        }


    }


    //TODO: request Permission Webview
    private void requestPermissionWebView(final String origin, final GeolocationPermissions.Callback callback) {
        mGeoLocationRequestOrigin = null;
        mGeoLocationCallBack = null;
        // Do we need to ask for permission?
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage(R.string.permission_rationale)
                        .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mGeoLocationRequestOrigin = origin;
                                mGeoLocationCallBack = callback;
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSION_REQUEST_LOCATION);
                            }
                        })
                        .show();
            } else {
                // No explanation needed, we can request th permission
                mGeoLocationRequestOrigin = origin;
                mGeoLocationCallBack = callback;
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSION_REQUEST_LOCATION);
            }
        } else {
            callback.invoke(origin, true, true);
        }
    }


    // 18/11/27

    private void showNotification(String info) {
        CharSequence text = "Location Service staring....";
        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
// Get a bitmap from drawable resource
        Bitmap bitmap = BitmapFactory.decodeResource(
                getApplication().getResources(),
                R.mipmap.ic_launcher_round
        );


        Notification notification = new NotificationCompat.Builder(this, "myChannel")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(info)
                .setLargeIcon(bitmap)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(info))
                .setContentText(info)  // the contents of the entry
                .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                .setOngoing(true)
                .setShowWhen(true)
                .build();
        //TODO: initialize Notification for GPS
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNM.notify(NOTIFICATION, notification);

    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("Service status", "Running");
                return true;
            }
        }
        Log.i ("Service status", "Not running");
        return false;
    }

    //


    // TODO: SEND GPS
    @JavascriptInterface
    public void sendGPS(final String type, final String cCode, final String driverId, final int vehicleIndex) {
        Log.e("ooooo", "Info=> " + type + " " + cCode + " " + driverId + " " + vehicleIndex);
        _startStep1();


        //TODO: register LocationMonitoringService
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        // CALL THE API OPERATION
                        //TODO: Initialized fusedLocation
                        client = LocationServices.getFusedLocationProviderClient(getApplicationContext());

                        final String latitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LATITUDE);
                        final String longitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LONGITUDE);
                        Log.d(TAG, "onReceive");
                        if (latitude != null && longitude != null) {

                            Toast.makeText(getApplicationContext(),"LocalBroacaseManger on Receiver: "+latitude+" : "+longitude,Toast.LENGTH_SHORT).show();

                            // TODO: make api request every time recieve location updates

                            //TODO: initialize Notification for GPS
                            final RetrofitInterface api = RetrofitClient.getClient().create(RetrofitInterface.class);
                            //TODO: check the type is test or production

                            String baseUrl = type.equals("pro")?"http://api.groundk.driver/groundk/":"http://210.115.182.242:8080/groundk/";
                            Log.e("ooooo","baseUrl=> "+baseUrl);
                            //TODO: Show notification that app is running
                            showNotification("Groundk Driver is Running");
                            webView.post(new Runnable() {
                                @Override
                                public void run() {
                                    checkGPSStatus("Running");
                                }
                            });


                            api.sendGPS(baseUrl,cCode, driverId, vehicleIndex, latitude, longitude).enqueue(new Callback<ResponseBody>() {

                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {
                                        Log.e("oooooXX", "post submitted to API." + response.body().toString());

                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    Log.e("oooooXX", "ERROR=> " + t.getLocalizedMessage());
                                    showSnackBarWithString(t.getLocalizedMessage(),
                                            android.R.string.ok, new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {


                                                }
                                            });
                                   // mNM.cancel(NOTIFICATION);
                                }
                            });


                            webView.loadUrl("javascript:getLocationFromAndroid('" + latitude + "','" + longitude + "')"); //if passing in an object. Mapping may need to take place


                        }
                    }
                }, new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST)
        );


        registerReceiver(broadcastReceiver, new IntentFilter(FirebaseService.TOKEN_BROADCAST));


    }


    //TODO: this will call when Javascript function is fired
    @SuppressLint("MissingPermission")
    @JavascriptInterface
    public void getCurrentLocation() {
        Log.e(TAG, "startBroadCastGPS");
        client = LocationServices.getFusedLocationProviderClient(this);

        if(checkPermissions()){
            client.getLastLocation().addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        webView.loadUrl("javascript:getLocationFromAndroid('" + location.getLatitude() + "','" + location.getLongitude() + "')"); //if passing in an object. Mapping may need to take place

                    }
                }
            });
        }else{
            requestPermissions();
        }






    }

    @JavascriptInterface
    public void sendFCM(){
        webView.post(new Runnable() {
            String fcmKey = SharePrefManager.getInstance(MainActivity.this).getToken();

            @Override
            public void run() {
                Log.e(TAG,"sendFCM"+fcmKey);

                webView.loadUrl("javascript:getFCMKey('" + fcmKey + "')"); //if passing in an object. Mapping may need to take place

            }
        });
    }


    @JavascriptInterface
    public void checkGPSStatus(String status){
        Log.e("ooooo","checkgps status "+ status);
        webView.loadUrl("javascript:checkGPSStatus('" + status + "')"); //if passing in an object. Mapping may need to take place

    }

    @JavascriptInterface
    public void stopBroadCastGPS(){
        //Stop location sharing service to app server.........
        Log.d(TAG,"stopBroadCasting....");

        Intent intent = new Intent(this,LocationMonitoringService.class);

        if(isMyServiceRunning(LocationMonitoringService.class)){

            stopService(intent);
            LocationMonitoringService.shouldContinue = false;

            webView.post(new Runnable() {
                @Override
                public void run() {
                    checkGPSStatus("Stopped");
                }
            });

            mAlreadyStartedService = false;
            showSnackbar(R.string.msg_location_service_stopped,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                        }
                    });
            mNM.cancel(NOTIFICATION);
            //Ends................................................

        }




    }

    @JavascriptInterface
    public void getGPSfromJS(String latitude,String longtitude){
        showSnackBarWithString("JS: Coordinates: "+latitude+" "+longtitude,
                android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                    }
                });
    }



    //TODO:refresh webview from webpage
    @JavascriptInterface
    public void refreshWebView(){
        Log.e(TAG,"webView reloaded");


        webView.post(new Runnable() {
            @Override
            public void run() {
                String historyUrl="";
                WebBackForwardList mWebBackForwardList = webView.copyBackForwardList();
                if (mWebBackForwardList.getCurrentIndex() > 0)
                    historyUrl = mWebBackForwardList.getItemAtIndex(mWebBackForwardList.getCurrentIndex()-1).getUrl();

                //TODO: Load last visited Url
                webView.loadUrl(historyUrl);
            }
        });
    }



}
